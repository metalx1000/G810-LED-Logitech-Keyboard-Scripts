#!/bin/bash
#original code https://delightlylinux.wordpress.com/2020/05/21/programming-the-logitech-g513-rgb-keyboard-lights-in-linux-with-g810-led/
#modified by Kris Occhipinti http://filmsbykris.com

function getColor()
{
  rgb=$(printf "%02x%02x%02x" $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)))
  echo $rgb
}

arrKeys=('esc'

    # Function F1-F12
    'f1' 'f2' 'f3' 'f4' 'f5' 'f6' 'f7' 'f8' 'f9' 'f10' 'f11' 'f12'

    # Print Screen Scroll Lock Pause
    'printscr' 'scrolllock' 'pause'

    'ins' 'del' 'home' 'end' 'pageup' 'pagedown'

    # Arrow keys
    'top' 'right' 'bottom' 'left'

    # Numpad
    'numlock' 'numslash' 'numasterisk' 'numminus' 'numplus' 'numenter'
    'num0' 'num1' 'num2' 'num3' 'num4' 'num5' 'num6' 'num7' 'num8' 'num9' 'num.'

    'tilde' {0..9} 'minus' 'equal' 'backspace'
    'tab' {a..z}
    'comma' 'period' 'slash' 'semicolon'
    'quote' 'open_bracket' 'close_bracket' 'backslash' 'enter' 'shiftright' 'shiftleft'
    'caps' 'winright' 'winleft' 'menu' 'space' 'capslock' 'ctrlleft' 'ctrlright'
    'altleft' 'altright'

    'gamemode' 'capsindicator' )

# Set all keys to off in case of "stuck" key colors
g810-led -a 000000


for key in ${arrKeys[*]}
do
  g810-led -k $key "$(getColor)"
done

g810-led -g gkeys $(getColor)
g810-led -g multimedia $(getColor)
g810-led -fx cycle logo ff

