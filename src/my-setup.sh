#!/bin/bash

g810-led -g modifiers ff0000
g810-led -g functions ff0000
g810-led -g fkeys ff00ff
g810-led -g keys 0000ff
g810-led -g arrows 00ff00
g810-led -g numeric 00ffff
g810-led -g media ffff00
g810-led -g gkeys ffffff
g810-led -g multimedia ffff00

echo -e "k w ff0000\nk a ff0000\nk s ff0000\nk d ff0000\nc" | g810-led -pp

g810-led -fx cycle logo ff
