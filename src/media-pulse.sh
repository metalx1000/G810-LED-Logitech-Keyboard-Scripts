#!/bin/bash
######################################################################
#Copyright (C) 2022  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

function getColor(){
  rgb=$(printf "%02x%02x%02x" $1 $2 $3)
  echo $rgb
}

for r in {0..255..16}
do
  for g in {0..255..16}
  do
    for b in {0..255..16}
    do
      echo "$r $g $b"
      g810-led -g multimedia $(getColor $r $g $b)
    done
  done
done
